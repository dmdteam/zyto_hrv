/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var Filter = function (B,A) {

    this.B=B;
    this.A=A;
    this.x=[0,0,0];
    this.y=[0,0,0];

    Filter.prototype.add_val=function add_val(input)
    {
        this.x[2]=this.x[1];
        this.x[1]=this.x[0];
        this.x[0]=input;

    //shift y values
	this.y[2]=this.y[1];
	this.y[1]=this.y[0];

	//convolution
	this.y[0]=this.B[0]*this.x[0];
	this.y[0]+=this.B[1]*this.x[1]-this.A[1]*this.y[1];
	this.y[0]+=this.B[2]*this.x[2]-this.A[2]*this.y[2];

	return this.y[0];
    }
    Filter.prototype.reset=function reset()
    {
        this.x=[0,0,0];
        this.y=[0,0,0];
    }
    
    Filter.prototype.filtfilt=function(arr)
    {
        Filter.reset();
        for(var i=0;i<arr.length;i++)
        {
            arr[i]=Filter.add_val(arr[i]);
        }
        Filter.reset();
        for(var i=arr.length-1;i>=0;i--)
        {
            arr[i]=Filter.add_val(arr[i]);
        }
        return arr;
    }
   
    
};