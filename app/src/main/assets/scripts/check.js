var data1 = {
    labels: "",
    datasets: [{

        label: "",
        radius: 0,
        showTooltip: false, //NEW OPTION DON"T NEED TO INCLUDE IT IF YOU WANT TO DISPLAY BUT WON"T HURT IF YOU DO
        data: data_p_pnnx_1,
        borderColor: "rgba( 255, 0, 0 ,1)"
    }, {

        label: "",
        radius: 0,
        showTooltip: false, //NEW OPTION DON"T NEED TO INCLUDE IT IF YOU WANT TO DISPLAY BUT WON"T HURT IF YOU DO
        data: data_p_pnnx_2,
        borderColor: "rgba( 0, 255, 0 ,1)"
    }, {

        label: "",
        radius: 0,
        showTooltip: false, //NEW OPTION DON"T NEED TO INCLUDE IT IF YOU WANT TO DISPLAY BUT WON"T HURT IF YOU DO
        data: data_p_pnnx_3,
        borderColor: "rgba( 0, 0, 255 ,1)"
    }, {

        label: "",
        radius: 0,
        showTooltip: false, //NEW OPTION DON"T NEED TO INCLUDE IT IF YOU WANT TO DISPLAY BUT WON"T HURT IF YOU DO
        data: data_p_pnnx_4,
        borderColor: "rgba( 0, 255, 255 ,1)"
    }, {

        label: "",
        radius: 0,
        showTooltip: false, //NEW OPTION DON"T NEED TO INCLUDE IT IF YOU WANT TO DISPLAY BUT WON"T HURT IF YOU DO
        data: data_p_pnnx_5,
        borderColor: "rgba( 89 , 89 , 128 ,1)"
    }]

};
var data2 = {
    labels: "",
    datasets: [{

        label: "",
        radius: 0,
        showTooltip: false, //NEW OPTION DON"T NEED TO INCLUDE IT IF YOU WANT TO DISPLAY BUT WON"T HURT IF YOU DO
        data: data_p_pnnb_1,
        borderColor: "rgba( 255, 0, 0 ,1)"
    }, {

             label: "",
             radius: 0,
             showTooltip: false, //NEW OPTION DON"T NEED TO INCLUDE IT IF YOU WANT TO DISPLAY BUT WON"T HURT IF YOU DO
             data: data_p_pnnb_2,
             borderColor: "rgba( 0, 255, 0 ,1)"
         }, {

             label: "",
             radius: 0,
             showTooltip: false, //NEW OPTION DON"T NEED TO INCLUDE IT IF YOU WANT TO DISPLAY BUT WON"T HURT IF YOU DO
             data: data_p_pnnb_3,
             borderColor: "rgba( 0, 0, 255 ,1)"
         }, {

             label: "",
             radius: 0,
             showTooltip: false, //NEW OPTION DON"T NEED TO INCLUDE IT IF YOU WANT TO DISPLAY BUT WON"T HURT IF YOU DO
             data: data_p_pnnb_4,
             borderColor: "rgba( 0, 255, 255 ,1)"
         }, {

             label: "",
             radius: 0,
             showTooltip: false, //NEW OPTION DON"T NEED TO INCLUDE IT IF YOU WANT TO DISPLAY BUT WON"T HURT IF YOU DO
             data: data_p_pnnb_5,
             borderColor: "rgba( 89 , 89 , 128 ,1)"
         }]

};