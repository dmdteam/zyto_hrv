package com.sensicardiac.zyto.zyto_state_HRV.detector;
import java.util.List;
import java.util.LinkedList;
public class signal_analysis {
	public signal_analysis()
	{
		
	}
	public static Integer[] findPeaks_win(double[] x,int span)
	{
		List<Integer> output=new LinkedList<Integer>();
		int l_min, l_max, r_min, r_max;
		int i, j, k;
		int is_peak;
		int dist;
		dist = (span + 1) / 2;
		int n=x.length;

		for (i=0; i<n; i++)
		{
			l_min = Math.max(i-dist+1, 0);
			l_max = i-1;
			r_min = i+1;
			r_max = Math.min(i+dist-1, n-1);

			is_peak = 1;

			/* left side */
			for (j=l_min; j<=l_max; j++)
				if (x[j] >= x[i])
				{
					is_peak = 0;
					break;
				}

			/* right side */
			if (is_peak == 1)
				for (j=r_min; j<=r_max; j++)
					if (x[j] >= x[i])
					{
						is_peak = 0;
						break;
					}

			if (is_peak == 1)
			{
				output.add(i);
				//
			}
		}
		//    for(int p=0;p<output.size();p++)
		//    {
		//        System.out.println("Java Peak: "+output.get(p));
		//    }
		// build the output vector



		/* copy and free the list */
		return output.toArray(new Integer[output.size()]);
	}
	
	public static Integer[] findPeaks_win(int[] x,int span)
	{
		List<Integer> output=new LinkedList<Integer>();
		int l_min, l_max, r_min, r_max;
		int i, j, k;
		int is_peak;
		int dist;
		dist = (span + 1) / 2;
		int n=x.length;

		for (i=0; i<n; i++)
		{
			l_min = Math.max(i-dist+1, 0);
			l_max = i-1;
			r_min = i+1;
			r_max = Math.min(i+dist-1, n-1);

			is_peak = 1;

			/* left side */
			for (j=l_min; j<=l_max; j++)
				if (x[j] >= x[i])
				{
					is_peak = 0;
					break;
				}

			/* right side */
			if (is_peak == 1)
				for (j=r_min; j<=r_max; j++)
					if (x[j] >= x[i])
					{
						is_peak = 0;
						break;
					}

			if (is_peak == 1)
			{
				output.add(i);
				//
			}
		}
		//    for(int p=0;p<output.size();p++)
		//    {
		//        System.out.println("Java Peak: "+output.get(p));
		//    }
		// build the output vector



		/* copy and free the list */
		return output.toArray(new Integer[output.size()]);
	}

}
