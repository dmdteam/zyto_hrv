package com.sensicardiac.zyto.zyto_state_HRV.SQL_DB;


import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Edward on 2017/02/27.
 */

public class HR_RECORDING {

    private static int _id;
    private static String _date;
    private static String _PPG;
    private static String _RMSSD;
    private static String _HF_LF;
    private static String _signal_data;

    public HR_RECORDING(){}
    public HR_RECORDING(int id, String date, String PPG, String RMSSD, String HF_LF, String signal_data){
        this._id = id;
        this._date = date;
        this._PPG = PPG;
        this._RMSSD = RMSSD;
        this._HF_LF = HF_LF;
        this._signal_data = signal_data;
    }
    public HR_RECORDING(String PPG, String RMSSD, String HF_LF, String signal_data){
        this._PPG = PPG;
        this._RMSSD = RMSSD;
        this._HF_LF = HF_LF;
        this._signal_data = signal_data;
    }


    public static int get_id() {
        return _id;
    }

    public static void set_id(int _id) {
        HR_RECORDING._id = _id;
    }

    public static String get_date() {
        return _date;
    }
//THIS FIELD IS JUST SO THAT SQLite can import the correct date format.
    public static void set_date(String date) {
        HR_RECORDING._date = date;
    }

    //IMPORTANT TO SET THIS AT RECORDING STAGE AS THE DATE OTHER WISE THINGS MIGHT GET MESSY
    public static void set_recording_Date(){
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        HR_RECORDING._date = dateFormat.format(date);
    }

    public static String get_Ppg() {
        return _PPG;
    }

    public static void set_Ppg(String Ppg) {
        _PPG = Ppg;
    }

    public static String get_Rmssd() {
        return _RMSSD;
    }

    public static void set_Rmssd(String Rmssd) {
        _RMSSD = Rmssd;
    }

    public static String get_HfLf() {
        return _HF_LF;
    }

    public static void set_HfLf(String HfLf) {
        _HF_LF = HfLf;
    }

    public static String get_signal_data() {
        return _signal_data;
    }

    public static void set_signal_data(String _signal_data) {
        HR_RECORDING._signal_data = _signal_data;
    }
}
