package com.sensicardiac.zyto.zyto_state_HRV.detector;

import java.util.LinkedList;
import java.util.List;
public class Beat {
	Point p0,p1,p2,p3,p4;
	int min_begin;
	int min_end;
	
//	Gauss_function pulse1;
	SysPulse pulse1;
	DiaPulse pulse2;
	Linear_function trend;
	public double derivative(double x)
	{
//		return pulse1.get_Deriv(x)+pulse2.get_Deriv(x);
		return pulse1.get_Deriv(x)+pulse2.get_Deriv(x)+trend.get_Deriv(x);
	}
	public Beat(int[] arr, int pos)
	{
	
		//Find starting point
		min_begin=pos;
		for(int j=0;j<30;j++)
		{
			if(arr[pos-j]<arr[min_begin])
			{
				min_begin=pos-j;
			}
		}
		
		//Find ending point
		min_end=pos;
		for(int j=0;j<100;j++)
		{
			if(arr[pos+j]<arr[min_end])
			{
				min_end=pos+j;
			}
		}
		p0=new Point(0, arr[min_begin]);
		p4=new Point(min_end-min_begin,arr[min_end] );
		
		//Find the trend line from the ends 
		trend = new Linear_function(p0, p4);
		
		
		//Convert array to double and subtract trend from it.
		int count=0;
		double[] d_arr=new double[min_end-min_begin];
//		System.out.println("Pulse 1: "+pulse1.getHeight()+" "+pulse1.getMean()+" "+pulse1.getStd());
		
		for(int i=min_begin;i<min_end;i++)
		{
			d_arr[count]=arr[i]-trend.get_val(count);
			count ++;
		}
		//Fit a gaussian pulse onto the data
		pulse1=new SysPulse(d_arr);
		
		//subtract fitted pulse from signal
		for(int i=0;i<d_arr.length;i++)
		{
			d_arr[i]=d_arr[i]-pulse1.get_val(i);
			if(d_arr[i]<0)
			{
				d_arr[i]=0;
			}
			
		}
		//fit second gaussian pulse to signal.
		pulse2=new DiaPulse(d_arr);
		
		//adjust height of pulse one to account for pulse 2
		pulse1.setHeight(pulse2);
//		System.out.println("Trend :"+trend.get_m()+" "+trend.get_c());
//		System.out.println("Pulse 1: "+pulse1.getHeight()+" "+pulse1.getMean()+" "+pulse1.getStd());
//		System.out.println("Pulse 2: "+pulse2.getHeight()+" "+pulse2.getMean()+" "+pulse2.getStd()+"\n");
		
		//find locations of the zero crossings in the derivative of the signal.
		List<Integer> zero_loc= new LinkedList<>();
		
		for(int i=0;i<d_arr.length-1;i++)
		{
			
			double first=derivative(i);
			
			int sign1=0;
			
			if(first>0)
			{
				sign1=1;
			}
			else
			{
				sign1=-1;
			}
			
			double second=derivative(i+1);
			int sign2=0;
			if(second>0)
			{
				sign2=1;
			}
			else
			{
				sign2=-1;
			}
			if(sign1!=sign2)
			{
				zero_loc.add(i);
			}
//			System.out.print((sign1-sign2)+",");
			
			
		}
		
//		for(int u =0;u<zero_loc.size();u++)
//		{
//			System.out.print(zero_loc.get(u)+",");
//			
//		}
//		System.out.println("");
		//Use pulse peaks and inflection point if less than three values are found.
		if(zero_loc.size()<3)
		{
//			System.out.println("using inflection");
			int min_point=pulse2.get_Mean();
			double min_val=derivative(min_point+3);
			while(derivative(min_point)<min_val)
			{
				min_val=derivative(min_point);
				min_point--;
			}
			zero_loc=new LinkedList<>();
			zero_loc.add(pulse1.get_Mean());
			zero_loc.add(min_point);
			zero_loc.add(pulse2.get_Mean());
			
		}
		//use first three values of peak if
		if(zero_loc.size()>3)
		{
		zero_loc=zero_loc.subList(0, 3);
		}
		//check here
		p1=new Point(zero_loc.get(0),arr[min_begin+zero_loc.get(0)] );
		p2=new Point(zero_loc.get(1),arr[min_begin+zero_loc.get(1)] );
		p3=new Point(zero_loc.get(2),arr[min_begin+zero_loc.get(2)] );
		
		pulse1.removeNan();
		pulse2.removeNan();
		trend.removeNan();

		
		
		
		
	}
	public String toString()
	{
		return "P0: "+p0.toString()+"\nP1: "+p1.toString()+"\nP2: "+p2.toString()+"\nP3: "+p3.toString()+"\nP4: "+p4.toString();
	}
	
	



}
