package com.sensicardiac.zyto.zyto_state_HRV.detector;

public class Linear_function {
	private double m;
	private double c;
	public Linear_function(Point p0, Point p1) {
		m=(p1.y-p0.y+0.0)/(p1.x-p0.x);
		c=p1.y*1.0-m*p1.x;
	}
	public double get_val(double x)
	{
		return m*x+c;
	}
	public double get_Deriv(double x)
	{
		return m;
	}
	public double get_m()
	{
		return m;
	}
	public double get_c()
	{
		return c;
	}

	public void removeNan()
	{
		if(Double.isNaN(m))
		{
			m=0;
		}
		if(Double.isNaN(c))
		{
			c=0;
		}
	}
	
	

}
