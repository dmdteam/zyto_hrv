package com.sensicardiac.zyto.zyto_state_HRV.States;

import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 * Created by davidfourie on 2017/02/20.
 */

public class recording_state extends DriverState {
    @Override
    public void recieve_message(String text) {
        Object obj = null;

        try {
            obj = Globals.parser.parse(text );
            JSONObject jsonObject = (JSONObject) obj;
            String command=(String)jsonObject.get("command");
            switch(command)
            {
                case "stop_recording":

                    Globals.driverStateManager.setState(new Device_connected_state());

                    break;

                default:

                    Globals.webSocket_server.sendError("Not a valid command for state");
                    break;





            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getStateName() {
        return "recording_state";
    }

    @Override
    public void onEntry() {
    Globals.toggleRecording();

        Globals.recorded_file=new int[Globals.pos];
        Globals.recording_done=false;

    }

    @Override
    public void onExit() {

        Globals.recording_done=true;
        Globals.recorded_file=new int[Globals.pos];
        System.arraycopy(Globals.buffer,0,Globals.recorded_file,0,Globals.pos);

        Globals.toggleRecording();

    }

    @Override
    public void stop_bluetooth() {

    }

    @Override
    public void web_socketClose() {
        Globals.mainActivity.cleanup_connection();
        Globals.driverStateManager.setState(new Unconnected_state());
    }

    @Override
    public void reset_recording()
    {

        Globals.webSocket_server.sendError("reset_recording");
        Globals.toggleRecording();
        Globals.recorded_file=new int[Globals.pos];
        Globals.toggleRecording();
        //send a message to the UI saying a reset has happen

    }

}
