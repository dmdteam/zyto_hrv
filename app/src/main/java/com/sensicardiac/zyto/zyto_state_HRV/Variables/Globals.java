package com.sensicardiac.zyto.zyto_state_HRV.Variables;

import android.os.Environment;
import android.util.Log;

import com.sensicardiac.zyto.zyto_state_HRV.Controller.MainActivity;
import com.sensicardiac.zyto.zyto_state_HRV.SQL_DB.DB_COMMANDS;
import com.sensicardiac.zyto.zyto_state_HRV.States.DriverStateManager;
import com.sensicardiac.zyto.zyto_state_HRV.Socket_Server.WebSocket_server;
import com.sensicardiac.zyto.zyto_state_HRV.States.streaming_state;
import com.sensicardiac.zyto.zyto_state_HRV.detector.feature_detect;
import com.sensicardiac.zyto.zyto_state_HRV.detector.signal_quality;

import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class Globals {
 public static DriverStateManager driverStateManager;
 public static WebSocket_server webSocket_server;
 public static JSONParser parser;
    public static MainActivity mainActivity;
    public static feature_detect ft;
    private static int recording_time;
    public static int[] buffer;
    public static int pos;
    private static boolean recoding;
    public static int[] recorded_file;
    public static boolean recording_done;
    public static signal_quality sq;


 public static void init(MainActivity mainActivity1)
 {
	 parser=new JSONParser();
	 driverStateManager=new DriverStateManager();
     mainActivity=mainActivity1;
     ft=new feature_detect();
     recording_time=60*100;
     buffer=new int[recording_time];
     recording_done=false;
     sq=new signal_quality();

     pos=0;
     recoding=false;

 }

 public static void setRecording_time(int time)
 {
     recording_time=time*100;
     buffer=new int[recording_time];
     pos=0;
 }
 public static void toggleRecording()
 {
     recoding=!recoding;
     pos=0;
 }
public static void add_to_buffer(int value)
{
    if(recoding)
    {
       if(pos<recording_time)
       {
           buffer[pos]=value;
           pos++;
       }
       else
       {
           driverStateManager.setState(new streaming_state());
       }
    }
}
public static void save_file(String name)
{
    if(recording_done)
    {
        SimpleDateFormat simpleDate =  new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        Date now = new Date();

        String timestamp = simpleDate.format(now);

        int REQUEST_WRITE_EXTERNAL_STORAGE=1;
        File Folder = new File (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) +File.separator +"Zyto/");
        if (!Folder.exists()){
            Folder.mkdirs();
        }
        try {
            File myFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) +File.separator +"Zyto/" + name+"_" + timestamp + ".csv");
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            String inputs = Arrays.toString(recorded_file);
            DB_COMMANDS._PPG = inputs;
            Log.d("Dat Points","3: PPG" + inputs.length());
            myOutWriter.append(inputs.substring(1,inputs.length()-1));
            myOutWriter.close();
            fOut.close();


        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
}
