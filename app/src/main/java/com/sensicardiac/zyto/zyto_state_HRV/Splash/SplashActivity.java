package com.sensicardiac.zyto.zyto_state_HRV.Splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.sensicardiac.zyto.zyto_state_HRV.Controller.MainActivity;


/**
 * Created by Edward on 2016/12/01.
 */


    public class SplashActivity extends Activity {
//    ProgressBar progressDialog;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            final Intent intent = new Intent(this, MainActivity.class);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {

                    startActivity(intent);
                    finish();
                }
            }, 3000);
//            requestWindowFeature(this.FEATURE_INDETERMINATE_PROGRESS);
            RelativeLayout layout = new RelativeLayout(this);
            ProgressBar progressBar = new ProgressBar(SplashActivity.this,null,android.R.attr.progressBarStyleLarge);
            progressBar.setIndeterminate(true);
            progressBar.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
            progressBar.setTop(150);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            layout.addView(progressBar,params);

            setContentView(layout);

        }
    }

