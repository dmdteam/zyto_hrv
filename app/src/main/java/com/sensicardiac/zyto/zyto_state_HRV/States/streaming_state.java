package com.sensicardiac.zyto.zyto_state_HRV.States;

import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class streaming_state extends DriverState {

	@Override
	public void recieve_message(String text) {

        Object obj = null;

        try {
            obj = Globals.parser.parse(text );
            JSONObject jsonObject = (JSONObject) obj;
            String command=(String)jsonObject.get("command");
            switch(command)
            {

				case "start_recording":
					Globals.driverStateManager.setState(new recording_state());
					break;


				case "recording_time":
					int time=Integer.parseInt((String)jsonObject.get("time"));
					Globals.setRecording_time(time);
					System.out.println("Setting recording time "+time);

					break;

				case "start_analysis":
					if(Globals.recording_done)
					{

						Globals.driverStateManager.setState(new analysis_state());
					}
					break;



                case "stop_stream":

//                    sg.pause();
                   Globals.mainActivity.notifyStop();
                    break;

                default:

                    Globals.webSocket_server.sendError("Not a valid command for state");
                    break;





            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
	}

	

	@Override
	public void web_socketClose() {

		//Make sure the class stops streaming
		
		//Make sure the class disconnects all the bluetooth devices
        Globals.mainActivity.cleanup_connection();
		Globals.driverStateManager.setState(new Unconnected_state());
		
	}

	@Override
	public String getStateName() {

		return "streaming_state";
	}

	@Override
	public void onEntry() {

		//Start steaming data to the websocket.
	}

	@Override
	public void onExit() {

		//stop streaming data to the websocket.

		
	}

	@Override
	public void stop_bluetooth() {
		///stop streaming activities
        Globals.mainActivity.cleanup_connection();
		Globals.driverStateManager.setState(new Bluetooth_off_state());
	}


	@Override
	public void reset_recording()
	{
		Globals.webSocket_server.sendError("quality_error");
		//Just send a message saying recording is rubbish
	}

}
