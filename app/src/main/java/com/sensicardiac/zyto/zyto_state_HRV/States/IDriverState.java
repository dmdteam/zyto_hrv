package com.sensicardiac.zyto.zyto_state_HRV.States;

public interface IDriverState {
void start_stream();
 void stop_stream();
void start_bluetooth();
void stop_bluetooth();
void get_devices();
void send_dev_num(int num);
void recieve_message(String text);
void chooseBluetoothState();
void send_confirm();
void web_socketClose();
    void get_state();
    void send_data(String text);
void reset_recording();


}
