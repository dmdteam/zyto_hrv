package com.sensicardiac.zyto.zyto_state_HRV.detector;

public class Point {
	int x;
	int y;
	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public Point(int x, double y) {
		super();
		this.x = x;
		this.y = (int)Math.round(y);
	}
	public String toString()
	{
		return "x: "+x+" y:"+y;
	}
	

	

}
