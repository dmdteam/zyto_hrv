package com.sensicardiac.zyto.zyto_state_HRV.States;

import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;
import com.sensicardiac.zyto.zyto_state_HRV.Not_Implemented_Exception.NotImplementedException;
import com.sensicardiac.zyto.zyto_state_HRV.Socket_Server.WebSocket_server;

import java.io.IOException;
import java.net.UnknownHostException;


public class Unconnected_state extends DriverState {

	@Override
	public String getStateName() {
		// TODO Auto-generated method stub
		return "unconnected";
	}

	@Override
	public void onEntry()  {
		if(Globals.webSocket_server==null)
		{
			try {
				Globals.webSocket_server=new WebSocket_server();
				Globals.webSocket_server.start();
				
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			try {
				Globals.webSocket_server.stop();
				Globals.webSocket_server= new WebSocket_server();
				Globals.webSocket_server.start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void onExit() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop_bluetooth() {
		System.out.println("Doesn't matter if unconnected class is disconnected from bluetooth");
	}

	public void chooseBluetoothState()
	{
		//Insert check for blue tooth here
		boolean bluetooth_check=Globals.mainActivity.isBluetoothEnabled();
		
		if(bluetooth_check)
		{
			Globals.driverStateManager.setState(new Bluetooth_on_state());
		}
		else
		{
			Globals.driverStateManager.setState(new Bluetooth_off_state());
		}
	}

	public void recieve_message(String text) {
		
		System.out.println("Unconnected class shouldn't recieve any messages");
		throw new NotImplementedException();
	}

	public void send_confirm() {
		
		System.out.println("Unconnected class shouldn't need to send anything.");
		
		
	}

	public void web_socketClose() {
		
		System.out.println("Unconnected class shouldn't be able to be closed");
		throw new NotImplementedException();
	}

	@Override
	public void get_state() {
		System.out.println("Unconnected class shouldn't be able to be send a state");
		throw new NotImplementedException();
		
	}
	

	
	

	

}
