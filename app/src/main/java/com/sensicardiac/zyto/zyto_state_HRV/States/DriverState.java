package com.sensicardiac.zyto.zyto_state_HRV.States;

import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;
import com.sensicardiac.zyto.zyto_state_HRV.Not_Implemented_Exception.NotImplementedException;

/**
 * Abstract class that all driver states inherit
 * @author davidfourie
 *
 */
public abstract class DriverState implements IDriverState {

	/**
	 * Get name of the current state
	 *
	 * @return The current state name
	 */
	public abstract String getStateName();

	/**
	 * This method is invoked when the state is entered
	 */
	public abstract void onEntry();

	/**
	 * This method is invoked when the state is left
	 */
	public abstract void onExit();

	public boolean equals(DriverState test) {
		if (test != null) {
			return this.getStateName().equals(test.getStateName());
		} else {
			return false;
		}

	}

	public boolean equals(String test) {
		if (test != null) {
			return this.getStateName().equals(test);
		} else {
			return false;
		}
	}

	/**
	 * Start sending the data to the client
	 */
	public void start_stream() {
		throw new NotImplementedException();
	}

	/**
	 * Stop sending the data to the client
	 */
	public void stop_stream() {
		throw new NotImplementedException();
	}

	/**
	 * Start the system's bluetooth backend
	 */
	public void start_bluetooth() {
		throw new NotImplementedException();
	}

	/**
	 * Stop the system's bluetooth backend
	 */
	public abstract void stop_bluetooth();

	public abstract void web_socketClose();

	/**
	 * Sends all the nearby bluetooth devices to the client
	 */
	public void get_devices() {
		throw new NotImplementedException();
	}

	/**
	 * connects to the selected stethoscope
	 */
	public void send_dev_num(int num) {
		throw new NotImplementedException();
	}


	public void chooseBluetoothState() {
		throw new NotImplementedException();
	}

	public void get_state() {
		Globals.webSocket_server.sendState(getStateName());

	}

	public void send_confirm() {
		// TODO Auto-generated method stub
		Globals.webSocket_server.sendState(getStateName());

	}

	public void send_data(String text) {
		throw new NotImplementedException();
	}

	public void reset_recording()
	{
		throw new NotImplementedException();
	}
}
