
package com.sensicardiac.zyto.zyto_state_HRV.Socket_Server;

import android.util.Log;

import com.sensicardiac.zyto.zyto_state_HRV.Controller.MainActivity;
import com.sensicardiac.zyto.zyto_state_HRV.Parser.PackageParser;
import com.sensicardiac.zyto.zyto_state_HRV.SQL_DB.DB_COMMANDS;
import com.sensicardiac.zyto.zyto_state_HRV.SQL_DB.HR_RECORDING;
import com.sensicardiac.zyto.zyto_state_HRV.SQL_DB.LOCAL_SQL_DB_Handler;
import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.simple.JSONObject;
public class WebSocket_server extends WebSocketServer{
	
	private WebSocket socket;
	public WebSocket_server() throws UnknownHostException {
		super(new InetSocketAddress(8887));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		// TODO Auto-generated method stub
		Log.d("websocket","connected");
		if(conn.getRemoteSocketAddress().getAddress().isLoopbackAddress())
		{

			this.socket=conn;
			Globals.driverStateManager.chooseBluetoothState();
			sendText("Connected");
			
		}
		else
		{
			conn.close();
		}
		
	}
	public void sendOxParams(PackageParser.OxiParams oxiParams)
	{
		JSONObject obj = new JSONObject();
		obj.put("command","set_oxi_param");
		obj.put("spo2",oxiParams.getSpo2());
		obj.put("pulse",oxiParams.getPulseRate());
		obj.put("pi",oxiParams.getPi());
		sendText(obj.toJSONString());
	}


	public void sendWavParams(int wave,int pulse,int spo2,int pi)
	{
		JSONObject obj = new JSONObject();
		obj.put("command","set_wave_param");
		obj.put("wav",wave);
		obj.put("pulse",pulse);
		obj.put("sp02",spo2);
		obj.put("pi",pi);
//        obj.put("pi",oxiParams.getPi());
		sendText(obj.toJSONString());
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		// TODO Auto-generated method stub
		this.socket=null;
		Globals.driverStateManager.web_socketClose();
		
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		Globals.driverStateManager.recieve_message(message);
		
	}

	@Override
	public void onError(WebSocket conn, Exception ex) {
		// TODO Auto-generated method stub
		
	}
	public void sendText(String text)
	{
		socket.send(text);
	}
	
	
	public void sendError(String message)
	{
		 JSONObject obj = new JSONObject();
	        obj.put("command","error");
	        obj.put("message",message);
	        sendText(obj.toJSONString());
	}
	
	
	public void sendState(String state_name)
	{
		 JSONObject obj = new JSONObject();
	        obj.put("command","state_val");
	        obj.put("state",state_name);
	        sendText(obj.toJSONString());
	}

}
