package com.sensicardiac.zyto.zyto_state_HRV.detector;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;
import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;



public class feature_detect {
    private List<Beat> beats;

    private int[] buffer;
    private Gson gson ;
    private int buffer_loc;

    private int len;
    private JSONParser jsp;
    private PSD psd;


    private class processSignal implements Runnable {
        private int[] proc_arr;

        public processSignal(int[] arr) {

            psd=new PSD();
            proc_arr = arr;
        }

        public void run() {
            beats.clear();
            fitBeats(proc_arr);



            // Convert object to JSON string and save into a file directly

            for (int i = 0; i < beats.size(); i++) {
//                System.out.println(beats.get(i).toString());
                String json = gson.toJson(beats.get(i));
                try {
                    JSONObject jso=(JSONObject)jsp.parse(json);
                    jso.put("command","set_signal_param");
                    Globals.webSocket_server.sendText(jso.toJSONString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }



            }
        }


//            ps.sendText("Hello");
    }



    public feature_detect() {

        beats = new LinkedList<>();
        len = 4 * 100;
        jsp=new JSONParser();
        reset();
        gson= new Gson();
        // TODO Auto-generated constructor stub
    }



    public void reset() {
        buffer = new int[len];

        buffer_loc = 0;


        beats.clear();
    }

    public void add_val(int val) {
        buffer[buffer_loc] = val;
        buffer_loc++;
        if (buffer_loc == len) {
            buffer_loc = 0;

            processSignal ps = new processSignal(Arrays.copyOf(buffer, len));
            Thread t = new Thread(ps);
            t.run();
        }
    }

    public double[] get_ssf(int[] arr) {
        SSF ssf = new SSF(15);
        return ssf.array_process(arr);
    }

    public Integer[] get_peaks(int[] arr) {

        SSF ssf = new SSF(15);
        double[] ssf_array = ssf.array_process(arr);
        Integer[] peaks = signal_analysis.findPeaks_win(ssf_array, 51);
        return peaks;
    }

    public boolean fitBeats(int[] arr) {
        reset();
        SSF ssf = new SSF(15);
        double[] ssf_array = ssf.array_process(arr);
        Integer[] peaks = signal_analysis.findPeaks_win(ssf_array, 51);

        for (int i = 0; i < peaks.length; i++) {

            if (peaks[i] > 50 && peaks[i] < arr.length - 150) {

                beats.add(new Beat(arr, peaks[i]));
            }
        }
        return beats.size() > 0;
    }


    public void fit_PSD()
    {
        int[] sys_points=get_SysPeakPoints();
        double[] x = new double[sys_points.length-1];
        double[] y = new double[sys_points.length-1];
        double mean=0;
        //Calculate HR
        for(int i=0;i<x.length;i++)
        {
            x[i]=sys_points[i]/100;
            y[i]=(60.0*100)/(sys_points[i+1]-sys_points[i]+0.0);
            mean=mean+y[i];
        }
        //Subtract mean from data
        mean=mean/y.length;
        for(int i=0;i<y.length;i++)
        {
            y[i]-=mean;
        }
        psd.psd_calculation(x, y, 0.02, 0.5, 10*x.length);

    }
    public double[] getFreqs()
    {
        return psd.getFreqs();
    }
    public double[] getPsd()
    {
        return psd.getPsd();
    }
    public double integrate(double start,double stop)
    {
        return psd.integrate(start, stop);
    }
    public void test()
    {
        psd.test();
    }
    public double RMSSD()
    {
        int[] sys_points=get_SysPeakPoints();


        double mean=0;
        //Calculate HR
        for(int i=0;i<sys_points.length-2;i++)
        {
            double hr1=(sys_points[i+1]-sys_points[i]+0.0)/100.0;
            double hr2=(sys_points[i+2]-sys_points[i+1]+0.0)/100.0;
            mean+=Math.pow(hr2-hr1, 2);
        }
        mean/=(sys_points.length-2);
        mean=Math.sqrt(mean);
        return mean;
    }
    public double pnnX(double x)
    {
        int[] sys_points=get_SysPeakPoints();
        double pnnX=0;
        //Calculate HR
        for(int i=0;i<sys_points.length-2;i++)
        {
            double hr1=(sys_points[i+1]-sys_points[i]+0.0)/100.0;
            double hr2=(sys_points[i+2]-sys_points[i+1]+0.0)/100.0;

            if(Math.abs(hr2-hr1)>x)
            {
                pnnX++;
            }

        }
        return (pnnX+0.0)/(sys_points.length-2);
    }
    public double[] getHR_params()
    {
        fit_PSD();
        double LF=integrate(0.04,0.15);
        double HF=integrate(0.18,0.4);
        double[] out = new double[5];
        out[0]=LF/HF;
        out[1]=RMSSD();
        out[2]=pnnX(0.02);
        out[3]=pnnX(0.05);
        out[4]=pnnX(0.07);
        return out;
    }


    public int[] get_startPoints() {
        int[] arr = new int[beats.size()];
        for (int i = 0; i < beats.size(); i++) {
            arr[i] = beats.get(i).p0.x + beats.get(i).min_begin;
        }
        return arr;
    }

    public int[] get_SysPeakPoints() {
        int[] arr = new int[beats.size()];
        for (int i = 0; i < beats.size(); i++) {
            arr[i] = beats.get(i).p1.x + beats.get(i).min_begin;
        }
        return arr;
    }

    public int[] get_NotchPoints() {
        int[] arr = new int[beats.size()];
        for (int i = 0; i < beats.size(); i++) {
            arr[i] = beats.get(i).p2.x + beats.get(i).min_begin;
        }
        return arr;
    }

    public int[] get_DiaPoints() {
        int[] arr = new int[beats.size()];
        for (int i = 0; i < beats.size(); i++) {
            arr[i] = beats.get(i).p3.x + beats.get(i).min_begin;
        }
        return arr;
    }

    public int[] get_EndPoints() {
        int[] arr = new int[beats.size()];
        for (int i = 0; i < beats.size(); i++) {
            arr[i] = beats.get(i).p4.x + beats.get(i).min_begin;
        }
        return arr;
    }


}
