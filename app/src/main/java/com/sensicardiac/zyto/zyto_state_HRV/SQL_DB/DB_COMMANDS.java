package com.sensicardiac.zyto.zyto_state_HRV.SQL_DB;

import android.util.Log;

import com.sensicardiac.zyto.zyto_state_HRV.Controller.MainActivity;

import org.json.simple.JSONObject;

import java.util.Arrays;

/**
 * Created by Edward on 2017/03/02.
 */

public class DB_COMMANDS {
   public static String _RMSSD;
    public static String _PPG;
    public static String _HF_LF;
    public static String _signal;

    public static void upload_new(){
        HR_RECORDING hr_recording = new HR_RECORDING();
        hr_recording.set_recording_Date();
        hr_recording.set_Ppg(_PPG);
        hr_recording.set_Rmssd(_RMSSD);
        hr_recording.set_HfLf(_HF_LF);
        hr_recording.set_signal_data(_signal);
        LOCAL_SQL_DB_Handler local_db = new LOCAL_SQL_DB_Handler(MainActivity.getmContext());
        local_db.add_Element(hr_recording);
        local_db.close();
        Log.d("params","SAVED");
    }

    public static JSONObject get_one_recording(int num){
        JSONObject obj = new JSONObject();
        LOCAL_SQL_DB_Handler db = new LOCAL_SQL_DB_Handler(MainActivity.getmContext());
        HR_RECORDING hr_recording = db.getRecording(num);
        obj.put("id",String.valueOf(hr_recording.get_id()));
        obj.put("date",hr_recording.get_date());
        obj.put("ppg",hr_recording.get_Ppg());
        obj.put("rmssd",hr_recording.get_Rmssd());
        obj.put("hflf",hr_recording.get_HfLf());
        obj.put("signal", hr_recording.get_signal_data());
        db.close();
        return obj;
    }

    public static JSONObject get_database_size(){
        LOCAL_SQL_DB_Handler db = new LOCAL_SQL_DB_Handler(MainActivity.getmContext());
        JSONObject obj = new JSONObject();
        int db_count = db.get_recording_Count();
        obj.put("count",db_count);
        db.close();
        return obj;
    }
}
