package com.sensicardiac.zyto.zyto_state_HRV.Not_Implemented_Exception;

public class NotImplementedException extends RuntimeException {
	private static final long serialVersionUID = 1L;

    public NotImplementedException(){}
}
