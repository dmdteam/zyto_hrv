package com.sensicardiac.zyto.zyto_state_HRV.detector;

public class DiaPulse extends Gauss_function{
	public DiaPulse(double arr[])
	{
		super(arr);
	}
	protected void fit(double arr[])
	{
//		System.out.println("I am also an inhereted version");
		height=-1;
		mean=0;
		for(int i=0;i<arr.length;i++)
		{
			if(arr[i]>height)
			{
				height=arr[i];
				mean=i;
			}
		}
		//find standard deviation
		std_deviation=0;
		for(int i=0;i<arr.length;i++)
		{
		std_deviation+=Math.pow(i-mean, 2)*arr[i];
		}
		std_deviation=Math.sqrt(std_deviation);
	}

}
