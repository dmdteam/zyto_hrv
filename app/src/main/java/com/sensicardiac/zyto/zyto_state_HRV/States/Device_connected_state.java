package com.sensicardiac.zyto.zyto_state_HRV.States;

import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 * Created by davidfourie on 2017/01/25.
 */

public class Device_connected_state extends DriverState {

    @Override
    public String getStateName() {
        // TODO Auto-generated method stub
        return "device_connected";
    }

    @Override
    public void onEntry() {
        // TODO Auto-generated method stub

        //turn on bluetooth automatically if possible.

    }

    @Override
    public void onExit() {
        // TODO Auto-generated method stub

    }
    @Override
    public void stop_bluetooth()
    {
        Globals.mainActivity.cleanup_connection();


        Globals.driverStateManager.setState(new Bluetooth_off_state());
    }

    @Override
    public void web_socketClose() {
        // TODO Auto-generated method stub

        //Make sure the class disconnects all the bluetooth devices
        Globals.driverStateManager.setState(new Unconnected_state());
    }

    @Override
    public void recieve_message(String text) {
        Object obj = null;

        try {
            obj = Globals.parser.parse(text );
            JSONObject jsonObject = (JSONObject) obj;
            String command=(String)jsonObject.get("command");
            switch(command)
            {

                case "start_stream":

                        Globals.mainActivity.notifyStart();
//                    sg.resume();


                    break;
                case "recording_time":
                    int time=Integer.parseInt((String)jsonObject.get("time"));
                    Globals.setRecording_time(time);

                    break;
                case "start_analysis":
                    if(Globals.recording_done)
                    {
                        Globals.driverStateManager.setState(new analysis_state());
                    }
                    break;

                default:

                    Globals.webSocket_server.sendError("Not a valid command for state");
                    break;





            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

}
