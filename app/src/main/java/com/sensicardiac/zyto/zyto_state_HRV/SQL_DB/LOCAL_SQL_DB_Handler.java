package com.sensicardiac.zyto.zyto_state_HRV.SQL_DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edward on 2017/02/27.
 */

public class LOCAL_SQL_DB_Handler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "History_View";
    private static final String TABLE_HR_HISTORY = "hr_history";
    private static final String KEY_ID = "id";
    private static final String KEY_DATE = "date";
    private static final String KEY_PPG = "ppg";
    private static final String KEY_RMSSD = "rmssd";
    private static final String KEY_HF_LF = "hf_lf";
    private static final String KEY_SIGNAL_DATA = "signal_data";

    public SQLiteDatabase sqlWrite = this.getWritableDatabase();
    public SQLiteDatabase sqlRead = this.getReadableDatabase();
    public SQLiteDatabase db;

    public LOCAL_SQL_DB_Handler(Context context){
        super (context,DATABASE_NAME,null,DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_HR_HISTORY_TABLE = "CREATE TABLE " + TABLE_HR_HISTORY + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_DATE + " TEXT," +
                KEY_PPG + " TEXT," +
                KEY_RMSSD + " TEXT," +
                KEY_HF_LF + " TEXT," +
                KEY_SIGNAL_DATA + " TEXT"
                + ")";
        sqLiteDatabase.execSQL(CREATE_HR_HISTORY_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_HR_HISTORY);
    }


    public void add_Element (HR_RECORDING recording){
        db = sqlWrite;
        ContentValues values = new ContentValues();
        values.put(KEY_DATE, recording.get_date());
        values.put(KEY_PPG, recording.get_Ppg());
        values.put(KEY_RMSSD,recording.get_Rmssd());
        values.put(KEY_HF_LF,recording.get_HfLf());
        values.put(KEY_SIGNAL_DATA,recording.get_signal_data());
        db.insert(TABLE_HR_HISTORY,null,values);

    }

    public HR_RECORDING getRecording(int id){
        db = sqlRead;
        HR_RECORDING rec = new HR_RECORDING();
        Cursor cursor = db.query(TABLE_HR_HISTORY, new String[]{KEY_ID,KEY_DATE,KEY_PPG,KEY_RMSSD,KEY_HF_LF,KEY_SIGNAL_DATA},KEY_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Log.d("Cursor", "CURSOR(0) : "+ cursor);
        rec.set_id(cursor.getInt(0));
        rec.set_date(cursor.getString(1));
        rec.set_Ppg((cursor.getString(2)));
        rec.set_Rmssd(cursor.getString(3));
        rec.set_HfLf(cursor.getString(4));
        rec.set_signal_data(cursor.getString(5));
        cursor.close();

        return rec;
    }
    public static List<HR_RECORDING> recordingList;
    public List<HR_RECORDING> getAll_Recordings(){
//        if (recordingList.size() > 0){
//            recordingList.clear();
//        }
       recordingList = new ArrayList<HR_RECORDING>();
        String selectQuery = "SELECT * FROM " + TABLE_HR_HISTORY;
        db = sqlRead;
        Cursor cursor = db.rawQuery(selectQuery,null);
        if (cursor.moveToFirst()){
            do{
                HR_RECORDING rec = new HR_RECORDING();
                rec.set_id(Integer.parseInt(cursor.getString(0)));
                rec.set_date(cursor.getString(1));
                rec.set_Ppg((cursor.getString(2)));
                rec.set_Rmssd(cursor.getString(3));
                rec.set_HfLf(cursor.getString(4));
                rec.set_signal_data(cursor.getString(5));
                recordingList.add(rec);
            } while (cursor.moveToNext());
            cursor.close();
        }
        db.close();
        return recordingList;
    }

    public int get_recording_Count(){
        String countQuery = "SELECT  * FROM " + TABLE_HR_HISTORY;
        db = sqlRead;
        Cursor cursor = db.rawQuery(countQuery,null);


        return  cursor.getCount();
    }

    public int Update_recording(HR_RECORDING recording){
        db = sqlWrite;
        ContentValues values = new ContentValues();
        int r = recording.get_id();
        values.put(KEY_DATE, recording.get_date());
        values.put(KEY_PPG, recording.get_Ppg());
        values.put(KEY_RMSSD,recording.get_Rmssd());
        values.put(KEY_HF_LF,recording.get_HfLf());
        values.put(KEY_SIGNAL_DATA,recording.get_signal_data());

        return db.update(TABLE_HR_HISTORY,values,KEY_ID + " = ?",new String[]{String.valueOf(r)});
    }

    public void Delete_Recodring(HR_RECORDING recording){
        db = sqlWrite;
        db.delete(TABLE_HR_HISTORY, KEY_ID + " = ?", new String[]{String.valueOf(recording.get_id())});
    }
}
