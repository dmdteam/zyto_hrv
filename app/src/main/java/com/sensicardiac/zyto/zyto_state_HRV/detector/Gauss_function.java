package com.sensicardiac.zyto.zyto_state_HRV.detector;

public class Gauss_function {
	protected double mean;
	protected double height;
	protected double std_deviation;
	public Gauss_function(double mean, double height, double std_deviation) {
		
		this.mean = mean;
		this.height = height;
		this.std_deviation = std_deviation;
	}
	public Gauss_function(double[] arr) {
		
		fit(arr);
			
	}
	public Gauss_function()
	{
		this.mean=0;
		this.height=1;
		this.std_deviation=0;
	}
	protected void fit(double[] arr)
	{
		System.out.println("I am not an inhereted version");
		//Find mean and height
		height=-1;
		mean=0;
		for(int i=0;i<arr.length;i++)
		{
			if(arr[i]>height)
			{
				height=arr[i];
				mean=i;
			}
		}
		if(height==0)
		{
			height=1;
		}
		//find standard deviation
		std_deviation=0;
		for(int i=0;i<arr.length;i++)
		{
		std_deviation+=Math.pow(i-mean, 2)*arr[i]/height;
		}
		std_deviation=Math.sqrt(std_deviation)/2;
	}
	

	public double getHeight()
	{
		return height;
	}
	public double getMean()
	{
		return mean;
	}
	public double getStd()
	{
		return std_deviation;
	}
	public double[] getArray(int length)
	{
		double[] out = new double[length];
		for(int i=0;i<length;i++)
		{
			out[i]=get_val(i);
		}
		return out;
	}
	public double get_Deriv(double x)
	{
		if(std_deviation==0)
		{
			std_deviation=40;
		}
		return height*Math.exp((-Math.pow(x-mean,2))/(std_deviation))*(1/std_deviation)*2*(x-mean);
	}
	
	public double get_val(double x)
	{
		if(std_deviation==0)
		{
			std_deviation=40;
		}
		return height*Math.exp((-Math.pow(x-mean,2))/(std_deviation));
	}
	
	public Point getPoint()
	{
		return new Point((int)Math.round(mean),(int)Math.round(height));
	}
	public int get_Mean()
	{
		return (int)Math.round(mean);
	}
	public void setHeight(Gauss_function pulse)
	{
		this.height=this.height-pulse.get_val(this.get_Mean());
	}

	public void removeNan()
	{
		if(Double.isNaN(mean))
		{
			mean=0;
		}
		if(Double.isNaN(height))
		{
			height=1;
		}
		if(Double.isNaN((std_deviation)))
		{
			std_deviation=40;
		}
	}

}
