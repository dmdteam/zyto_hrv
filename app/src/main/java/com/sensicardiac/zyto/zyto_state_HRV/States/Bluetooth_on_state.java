package com.sensicardiac.zyto.zyto_state_HRV.States;

import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class Bluetooth_on_state extends DriverState {
    private boolean device_found;
	@Override
	public String getStateName() {
		// TODO Auto-generated method stub
		return "bluetooth_on";
	}

	@Override
	public void onEntry() {
		// TODO Auto-generated method stub
        device_found=false;
		//turn on bluetooth automatically if possible.

        //Check to see bluetooth is off
        if(!Globals.mainActivity.isBluetoothEnabled())
        {
            Globals.driverStateManager.setState(new Bluetooth_off_state());
        }
		
	}

	@Override
	public void onExit() {
		// TODO Auto-generated method stub
		
	}
	@Override
    public void stop_bluetooth()
    {


        Globals.driverStateManager.setState(new Bluetooth_off_state());
    }

    public void start_bluetooth()
    {
        //do nothing :)
    }

	public void recieve_message(String text) {
		Object obj = null;
	
        try {
            obj = Globals.parser.parse(text );
            JSONObject jsonObject = (JSONObject) obj;
            String command=(String)jsonObject.get("command");
            switch(command)
            {
                case "get_state":
                	get_state();
                	break;
                case "stop_bluetooth":
                	Globals.mainActivity.turn_off_bluetooth();
                   // Globals.driverStateManager.setState(new Bluetooth_off_state());
                    break;
                case "get_devices":

                  Globals.mainActivity.scanLeDevice_find(true);
                    break;
                case "send_dev_num":
                    int dev_num= 0;
                    try {
                        dev_num = Integer.parseInt((String)jsonObject.get("dev_num"));

                        if(Globals.mainActivity.selectDevice(dev_num))
                        {
                            device_found=true;



                        }
                        else
                        {
                            Globals.webSocket_server.sendError("Number not valid");
                        }

                        //mainActivity.selectDevice(dev_num);
                    } catch (NumberFormatException e) {
                        Globals.webSocket_server.sendError("Number not valid");
                    }
                    break;
              default:
            
              	Globals.webSocket_server.sendError("Not a valid command for state");
              	break;
            	




            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
		
	}

	

	public void web_socketClose() {
		// TODO Auto-generated method stub
		
		//Make sure the class disconnects all the bluetooth devices 
		Globals.driverStateManager.setState(new Unconnected_state());
	}
    public void send_data(String text)
    {
        Globals.webSocket_server.sendText(text);
    }

	

}
